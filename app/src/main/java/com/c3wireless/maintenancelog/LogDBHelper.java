package com.c3wireless.maintenancelog;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.c3wireless.maintenancelog.LogDBContract.EquipmentEntry;
import com.c3wireless.maintenancelog.LogDBContract.LogEntry;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by nock on 12/07/16.
 */
public class LogDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MaintLog.db";
    private static final List<String> RECORD_TYPES = Arrays.asList(
            "Compressor", "Valve", "Gauge", "Drainage", "Pump");

    public LogDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + EquipmentEntry.TABLE_NAME + " (" +
                EquipmentEntry._ID + " INTEGER PRIMARY KEY," +
                EquipmentEntry.COLUMN_NAME_EQUIPMENT_NAME + " TEXT, " +
                EquipmentEntry.COLUMN_NAME_CHECK_DUE + " DATETIME);");
        db.execSQL("CREATE TABLE " + LogEntry.TABLE_NAME + " (" +
                LogEntry._ID + " INTEGER PRIMARY KEY," +
                LogEntry.COLUMN_NAME_EQUIPMENT_ID + " REFERENCES " + EquipmentEntry.TABLE_NAME + ", " +
                LogEntry.COLUMN_NAME_NOTES + " TEXT, " +
                LogEntry.COLUMN_NAME_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                LogEntry.COLUMN_NAME_CHECKED_BY + " TEXT);");
        insertDemoData(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + EquipmentEntry.TABLE_NAME + ";");
        db.execSQL("DROP TABLE IF EXISTS " + LogEntry.TABLE_NAME + ";");
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertEquipment(SQLiteDatabase db, String name, Date next_check) {
        ContentValues values = new ContentValues();
        values.put(EquipmentEntry.COLUMN_NAME_EQUIPMENT_NAME, name);
        values.put(EquipmentEntry.COLUMN_NAME_CHECK_DUE, next_check.toString());
        return db.insert(
                EquipmentEntry.TABLE_NAME,
                null,
                values);
    }

    public long insertLog(SQLiteDatabase db, String notes, String checked_by, long equipment_id) {
        ContentValues logValues = new ContentValues();
        logValues.put(LogEntry.COLUMN_NAME_NOTES, notes);
        logValues.put(LogEntry.COLUMN_NAME_CHECKED_BY, checked_by);
        logValues.put(LogEntry.COLUMN_NAME_EQUIPMENT_ID, equipment_id);
        long log_id = db.insert(
                LogEntry.TABLE_NAME,
                null,
                logValues
        );

        ContentValues eqValues = new ContentValues();
        Calendar next_check = Calendar.getInstance();
        next_check.setTime(new Date());
        Random RandomGenerator = new Random();
        next_check.add(Calendar.MINUTE, RandomGenerator.nextInt(300));
        SimpleDateFormat iso8601 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        eqValues.put(EquipmentEntry.COLUMN_NAME_CHECK_DUE, iso8601.format(next_check.getTime()));
        db.update(
                EquipmentEntry.TABLE_NAME,
                eqValues,
                EquipmentEntry._ID + " = ?",
                new String[]{String.valueOf(equipment_id)}
        );
        return log_id;
    }

    private void insertDemoData(SQLiteDatabase db) {
        Random RandomGenerator = new Random();
        int initial_records = RandomGenerator.nextInt(10);
        for (int i = 0; i < initial_records; i++) {
            int type_idx = RandomGenerator.nextInt(RECORD_TYPES.size());
            int uniq_id = RandomGenerator.nextInt(255);
            String name = RECORD_TYPES.get(type_idx) + " " +
                    Integer.toHexString(uniq_id).toUpperCase();
            Calendar next_check = Calendar.getInstance();
            next_check.setTime(new Date());
            next_check.add(Calendar.MINUTE, RandomGenerator.nextInt(300));
            insertEquipment(db, name, next_check.getTime());
        }
    }
}
