package com.c3wireless.maintenancelog;

import android.provider.BaseColumns;

/**
 * Created by nock on 12/07/16.
 */
public final class LogDBContract {
    public LogDBContract() {
    }

    public static abstract class EquipmentEntry implements BaseColumns {
        public static final String TABLE_NAME = "equipment";
        public static final String COLUMN_NAME_EQUIPMENT_NAME = "name";
        public static final String COLUMN_NAME_CHECK_DUE = "next";
    }

    public static abstract class LogEntry implements BaseColumns {
        public static final String TABLE_NAME = "log_entries";
        public static final String COLUMN_NAME_EQUIPMENT_ID = "equipment_id";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_NOTES = "notes";
        public static final String COLUMN_NAME_CHECKED_BY = "checked_by";
    }
}
