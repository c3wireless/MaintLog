package com.c3wireless.maintenancelog;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.c3wireless.maintenancelog.LogDBContract.EquipmentEntry;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nock on 14/07/16.
 */
public class EquipmentListCursorAdapter extends SimpleCursorAdapter {
    private int layout;

    public EquipmentListCursorAdapter(Context context, int layout, Cursor c, String[] from,
                                      int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.layout = layout;
    }

    @Override
    public View newView(Context context, Cursor c, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        return inflater.inflate(layout, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor c) {
        String eq_name = c.getString(c.getColumnIndex(EquipmentEntry.COLUMN_NAME_EQUIPMENT_NAME));
        String next_check = c.getString(c.getColumnIndex(EquipmentEntry.COLUMN_NAME_CHECK_DUE));
        SimpleDateFormat iso8601 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date date;
        try {
            date = iso8601.parse(next_check);
        } catch (ParseException e) {
            date = null;
        }
        String rel_date = next_check;
        if (date != null) {
            long when = date.getTime();
            rel_date = DateUtils.getRelativeTimeSpanString(
                    when, new Date().getTime(), DateUtils.MINUTE_IN_MILLIS).toString();
        }
        TextView tv = (TextView) view.findViewById(R.id.name);
        tv.setText(eq_name);
        TextView tv_due = (TextView) view.findViewById(R.id.due);
        tv_due.setText(context.getString(R.string.rel_date_head, rel_date));
    }
}
