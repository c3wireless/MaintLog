package com.c3wireless.maintenancelog;

import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.c3wireless.maintenancelog.LogDBContract.EquipmentEntry;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    public static final String[] PROJECTION = {
            EquipmentEntry._ID,
            EquipmentEntry.COLUMN_NAME_EQUIPMENT_NAME,
            EquipmentEntry.COLUMN_NAME_CHECK_DUE
    };
    private static final String TAG = "MainActivity";
    public static String SORT_ORDER = EquipmentEntry.COLUMN_NAME_CHECK_DUE + " ASC";
    public SimpleCursorAdapter mAdapter;
    private Timer updateTimer;
    private MainActivity mainActivityContext;
    private final BroadcastReceiver refreshNoticeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v(TAG, "refresh triggered");
            getLoaderManager().restartLoader(0, null, mainActivityContext);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String[] fromColumns = {
                EquipmentEntry.COLUMN_NAME_EQUIPMENT_NAME,
                EquipmentEntry.COLUMN_NAME_CHECK_DUE};
        int[] toViews = {
                R.id.name,
                R.id.due};
        mAdapter = new EquipmentListCursorAdapter(this, R.layout.list_item, null, fromColumns, toViews, 0);
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(parent.getContext(), AddLogActivity.class);
                intent.putExtra(AddLogActivity.EQUIPMENT_ID_KEY, id);
                startActivity(intent);
            }
        });
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivityContext = this;
        this.registerReceiver(refreshNoticeReceiver, new IntentFilter("refreshtimer"));
        getLoaderManager().restartLoader(0, null, this);
        updateTimer = new Timer();
        final MainActivity that = this;
        updateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent("refreshtimer");
                        sendBroadcast(intent);
                    }
                });
            }
        }, 0, 60000);
    }

    @Override
    public void onPause() {
        updateTimer.cancel();
        unregisterReceiver(refreshNoticeReceiver);
        super.onPause();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = Uri.withAppendedPath(LogDBProvider.CONTENT_URI, EquipmentEntry.TABLE_NAME);
        return new CursorLoader(this, uri, PROJECTION, null, null, SORT_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
        TextView tv = (TextView) findViewById(R.id.empty);
        if (c.getCount() == 0) {
            tv.setSystemUiVisibility(TextView.VISIBLE);
        } else {
            tv.setSystemUiVisibility(TextView.INVISIBLE);
        }
        mAdapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
