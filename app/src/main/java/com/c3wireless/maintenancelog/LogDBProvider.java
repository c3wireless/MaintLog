package com.c3wireless.maintenancelog;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

/**
 * Created by nock on 12/07/16.
 */
public class LogDBProvider extends ContentProvider {
    private static final String AUTHORITY = "com.c3wireless.maintenancelog.logdbprovider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    private LogDBHelper mOpenHelper;

    public static String getTableName(Uri uri) {
        String value = uri.getPath();
        return value.replace("/", "");
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new LogDBHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        String table = getTableName(uri);
        SQLiteDatabase database = mOpenHelper.getReadableDatabase();
        return database.query(table, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selectionClause, String[] selectionArgs) {
        String table = getTableName(uri);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        return db.update(table, values, selectionClause, selectionArgs);
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        String table = getTableName(uri);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long id = db.insert(table, null, values);
        return Uri.withAppendedPath(uri, String.valueOf(id));
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        String table = getTableName(uri);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        return db.delete(table, selection, selectionArgs);
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }
}
