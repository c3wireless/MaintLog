package com.c3wireless.maintenancelog;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.c3wireless.maintenancelog.LogDBContract.EquipmentEntry;

public class AddLogActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String EQUIPMENT_ID_KEY = "eid";
    public static final String[] PROJECTION = {
            EquipmentEntry._ID,
            EquipmentEntry.COLUMN_NAME_EQUIPMENT_NAME,
    };
    private static final String SELECTION = EquipmentEntry._ID + "=?";
    private long EQUIPMENT_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_log);
        Toolbar toolbar = (Toolbar) findViewById(R.id.add_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Intent intent = getIntent();
        EQUIPMENT_ID = intent.getLongExtra(EQUIPMENT_ID_KEY, -1);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = Uri.withAppendedPath(LogDBProvider.CONTENT_URI, EquipmentEntry.TABLE_NAME);
        String[] selection_args = {Long.toString(EQUIPMENT_ID)};
        return new CursorLoader(this, uri, PROJECTION, SELECTION, selection_args, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
        c.moveToFirst();
        String eq_name = c.getString(c.getColumnIndex(
                LogDBContract.EquipmentEntry.COLUMN_NAME_EQUIPMENT_NAME));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.log_header, eq_name));
        }
    }

    public void saveLog(View view) {
        EditText editText = (EditText) findViewById(R.id.editText);
        String notes = editText.getText().toString();
        LogDBHelper dbHelper = new LogDBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        dbHelper.insertLog(db, notes, "Demo User", EQUIPMENT_ID);
        Toast.makeText(getApplicationContext(), R.string.save_text, Toast.LENGTH_SHORT).show();
        finish();
    }
}
